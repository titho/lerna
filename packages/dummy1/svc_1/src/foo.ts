import * as p1 from "@syankov/common";
import * as p2 from "../../../dummy2/svc_2/src/foo"

export function fn1_1() {
    console.log("SVC1_1 is called!");
    p1.fn();
}

export function fn1_2() {
    console.log("SVC1_2 is called!");
    p2.fn2_2();
}