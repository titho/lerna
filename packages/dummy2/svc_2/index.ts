import * as p2 from "./src/foo"
import express, { Application } from "express";

const app: express.Application = express();

app.use("/1", p2.fn2_1);
app.use("/2", p2.fn2_2);

app.listen(3000, () => {
    // Success callback
    console.log(`Listening at http://localhost:3000/`);
});