import * as p1 from "@syankov/common";
import * as p2 from "../../../dummy1/svc_1/src/foo";

export function fn2_1() {
    console.log("SVC2_1 is called");
    p2.fn1_1();
}

export function fn2_2(){
    console.log("SVC2_2 is called");
    p1.fn();
}

